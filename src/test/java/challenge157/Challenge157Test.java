package challenge157;

import general.Constants;

import java.io.File;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.powermock.reflect.internal.WhiteboxImpl;

public class Challenge157Test {

	private static Challenge157 challenge157;
	private static final String FILE_NAME = "TemporaryFileName.txt";

	@Rule
	public TemporaryFolder tempFolder = new TemporaryFolder();

	public void initializeFile() {
		try {
			final File temporaryFile = tempFolder.newFile(FILE_NAME);

			List<String> content = Arrays.asList("*** ***", "* *  **", "* **  *", "*    **", "*  * **", "* **  *", "***** *");

			Path filePath = Paths.get(temporaryFile.getAbsolutePath());

			Files.write(filePath, content, Charset.forName(Constants.CHARSET_UTF_8));

			challenge157 = new Challenge157();
			challenge157.loadMaze(filePath.toString());

		} catch (Exception e) {
			Assert.fail("File initialization fail.");
		}
	}

	@Test
	public void findEntryPointTestFound() throws Exception {

		Challenge157 challenge157 = new Challenge157();

		String[] firstLine = new String[] { " * ", "", " * " };

		Integer entrancePoint = WhiteboxImpl.invokeMethod(challenge157, "findEntryPoint", (Object) firstLine);

		Assert.assertSame(1, entrancePoint);
	}

	@Test
	public void findEntryPointTestNotFound() throws Exception {

		Challenge157 challenge157 = new Challenge157();

		String[] firstLine = new String[] { " * ", " * ", " * " };

		Integer entrancePoint = WhiteboxImpl.invokeMethod(challenge157, "findEntryPoint", (Object) firstLine);

		Assert.assertNull(entrancePoint);
	}

	@Test
	public void isTopCellEmptyUpperLine() throws Exception {
		initializeFile();

		boolean empty = WhiteboxImpl.invokeMethod(challenge157, "isTopCellEmpty", 0, 0);

		Assert.assertFalse(empty);
	}

	@Test
	public void isTopCellEmptyTrue() throws Exception {
		initializeFile();

		boolean empty = WhiteboxImpl.invokeMethod(challenge157, "isTopCellEmpty", 1, 3);

		Assert.assertTrue(empty);
	}

	@Test
	public void isTopCellEmptyFalse() throws Exception {
		initializeFile();

		boolean empty = WhiteboxImpl.invokeMethod(challenge157, "isTopCellEmpty", 1, 1);

		Assert.assertFalse(empty);
	}

	@Test
	public void isBottomCellEmptyLowerLine() throws Exception {
		initializeFile();

		boolean empty = WhiteboxImpl.invokeMethod(challenge157, "isBottomCellEmpty", 6, 6);

		Assert.assertFalse(empty);
	}

	@Test
	public void isBottomCellEmptyTrue() throws Exception {
		initializeFile();

		boolean empty = WhiteboxImpl.invokeMethod(challenge157, "isBottomCellEmpty", 5, 5);

		Assert.assertTrue(empty);
	}

	@Test
	public void isBottomCellEmptyLowerFalse() throws Exception {
		initializeFile();

		boolean empty = WhiteboxImpl.invokeMethod(challenge157, "isBottomCellEmpty", 5, 6);

		Assert.assertFalse(empty);
	}

	@Test
	public void isRightCellEmptyRightColumn() throws Exception {
		initializeFile();

		boolean empty = WhiteboxImpl.invokeMethod(challenge157, "isRightCellEmpty", 0, 6);

		Assert.assertFalse(empty);
	}

	@Test
	public void isRightCellEmptyTrue() throws Exception {
		initializeFile();

		boolean empty = WhiteboxImpl.invokeMethod(challenge157, "isRightCellEmpty", 1, 0);

		Assert.assertTrue(empty);
	}

	@Test
	public void isRightCellEmptyFalse() throws Exception {
		initializeFile();

		boolean empty = WhiteboxImpl.invokeMethod(challenge157, "isRightCellEmpty", 1, 1);

		Assert.assertFalse(empty);
	}

	@Test
	public void isLeftCellEmptyLeftColumn() throws Exception {
		initializeFile();

		boolean empty = WhiteboxImpl.invokeMethod(challenge157, "isLeftCellEmpty", 0, 0);

		Assert.assertFalse(empty);
	}

	@Test
	public void isLeftCellEmptyTrue() throws Exception {
		initializeFile();

		boolean empty = WhiteboxImpl.invokeMethod(challenge157, "isLeftCellEmpty", 0, 4);

		Assert.assertTrue(empty);
	}

	@Test
	public void isLeftCellEmptyFalse() throws Exception {
		initializeFile();

		boolean empty = WhiteboxImpl.invokeMethod(challenge157, "isLeftCellEmpty", 0, 5);

		Assert.assertFalse(empty);
	}

}
