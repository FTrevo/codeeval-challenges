package general;

public class Constants {

	public static final String PATH_TO_CHALLENGE157_ENTRANCE_FILE = "H:/workspaceEclipse/codeeval-challenges/documentation/challenge_157/157EntranceFile.txt";
	public static final String CHALLENGE157_WALL = " * ";
	public static final String CHALLENGE157_EMPTY = "   ";

	public static final String CHARSET_UTF_8 = "UTF-8";

}
