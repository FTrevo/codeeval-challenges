package general;

import java.util.Collection;

public class StringUtils {

	public static boolean isValidString(String value) {
		return value != null && !"".equals(value);
	}

	public static boolean isEmptyString(String value) {
		return value != null && "".equals(value.trim());
	}

	public static boolean isValidCollection(Collection<?> collection) {
		return collection != null && !collection.isEmpty();
	}
}
