package general;

import challenge157.Challenge157;

public class Main {

	public static void main(String[] args) {

		Challenge157 challenge157 = new Challenge157();
		challenge157.loadMaze(Constants.PATH_TO_CHALLENGE157_ENTRANCE_FILE);
		challenge157.printMaze();

	}

}
