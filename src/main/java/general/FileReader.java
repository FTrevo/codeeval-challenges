package general;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

public class FileReader {

	public static ArrayList<String> readFile(String path) throws IOException {
		ArrayList<String> retorno = new ArrayList<String>();

		Path caminho = Paths.get(path);

		if (StringUtils.isValidString(path) || Files.exists(caminho)) {
			retorno = (ArrayList<String>) Files.readAllLines(caminho);
		}
		return retorno;
	}
}
