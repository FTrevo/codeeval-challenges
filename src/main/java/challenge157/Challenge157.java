package challenge157;

import general.Constants;
import general.FileReader;
import general.StringUtils;

import java.util.ArrayList;

public class Challenge157 {

	private Integer lineQuantity;
	private Integer columnQuantity;

	private String[][] maze;

	public void loadMaze(String path) {
		try {
			ArrayList<String> mazeArray = FileReader.readFile(path);

			if (StringUtils.isValidCollection(mazeArray)) {
				lineQuantity = mazeArray.size();
				columnQuantity = mazeArray.get(0).length();

				maze = new String[lineQuantity][columnQuantity];

				for (int i = 0; i < mazeArray.size(); i++) {
					String currentLine = mazeArray.get(i);

					for (int j = 0; j < currentLine.length(); j++) {
						if (StringUtils.isEmptyString("" + currentLine.charAt(j))) {
							maze[i][j] = Constants.CHALLENGE157_EMPTY;
						} else {
							maze[i][j] = Constants.CHALLENGE157_WALL;
						}
					}
				}
			}
		} catch (Exception e) {
			System.out.println("Exception thrown, please check the input file,");
		}
	}

	public void printMaze() {
		for (String[] row : maze) {
			for (String column : row) {
				System.out.print(" " + column + "");
			}
			System.out.print("\n");
		}
	}

	public void processMaze() {
		Integer entryPoint = findEntryPoint(maze[0]);

	}

	private Integer findEntryPoint(String[] line) {

		for (int i = 0; i < line.length; i++) {
			if (!Constants.CHALLENGE157_WALL.equals(line[i])) {
				return i;
			}
		}

		return null;
	}

	private ArrayList<Integer[]> findEmptyNeighbors(Integer currentLine, Integer currentColumn) {
		ArrayList<Integer[]> neighborsFound = new ArrayList<Integer[]>();

		if (isTopCellEmpty(currentLine, currentColumn)) {
			neighborsFound.add(new Integer[] { currentLine - 1, currentColumn });
		}

		if (isBottomCellEmpty(currentLine, currentColumn)) {
			neighborsFound.add(new Integer[] { currentLine + 1, currentColumn });
		}

		if (isRightCellEmpty(currentLine, currentColumn)) {
			neighborsFound.add(new Integer[] { currentLine, currentColumn + 1 });
		}

		return neighborsFound;
	}

	private boolean isTopCellEmpty(Integer currentLine, Integer currentColumn) {
		if (currentLine != 0 && StringUtils.isEmptyString(maze[currentLine - 1][currentColumn])) {
			return true;
		}
		return false;
	}

	private boolean isBottomCellEmpty(Integer currentLine, Integer currentColumn) {
		if (currentLine != lineQuantity - 1 && StringUtils.isEmptyString(maze[currentLine + 1][currentColumn])) {
			return true;
		}
		return false;
	}

	private boolean isRightCellEmpty(Integer currentLine, Integer currentColumn) {
		if (currentColumn != columnQuantity - 1 && StringUtils.isEmptyString(maze[currentLine][currentColumn + 1])) {
			return true;
		}
		return false;
	}

	private boolean isLeftCellEmpty(Integer currentLine, Integer currentColumn) {
		if (currentColumn != 0 && StringUtils.isEmptyString(maze[currentLine][currentColumn - 1])) {
			return true;
		}
		return false;
	}
}
